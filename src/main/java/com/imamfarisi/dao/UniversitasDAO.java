package com.imamfarisi.dao;
/* 
author imam-pc 
created on 21/04/2020
*/

import com.imamfarisi.model.Universitas;

import java.util.List;

public interface UniversitasDAO {

    void insertData(Universitas data);

    void updateData(Universitas data);

    void deleteData(Universitas data);

    void deleteAllData();

    List<Universitas> getAllData();
}
