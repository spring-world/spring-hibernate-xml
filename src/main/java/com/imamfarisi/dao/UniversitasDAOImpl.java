package com.imamfarisi.dao;

import com.imamfarisi.model.Universitas;
import org.hibernate.SessionFactory;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;

public class UniversitasDAOImpl implements UniversitasDAO {

    private TransactionTemplate transactionTemplate;
    private SessionFactory sessionFactory;

    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void insertData(Universitas data) {
        transactionTemplate.executeWithoutResult(val -> {
            sessionFactory.getCurrentSession().save(data);
        });
    }

    public void updateData(Universitas data) {
        transactionTemplate.executeWithoutResult(val -> {
            sessionFactory.getCurrentSession().merge(data);
        });
    }

    public void deleteData(Universitas data) {
        transactionTemplate.executeWithoutResult(val -> {
            sessionFactory.getCurrentSession().remove(data);
        });
    }

    public void deleteAllData() {
        transactionTemplate.executeWithoutResult(val -> {
            String sql = "DELETE FROM Universitas";
            sessionFactory.getCurrentSession().createQuery(sql).executeUpdate();
        });
    }

    public List<Universitas> getAllData() {
        List<Universitas> listUniv = new ArrayList<>();
        listUniv = transactionTemplate.execute(val -> {
            List<Universitas> list = sessionFactory.getCurrentSession().createQuery("from Universitas", Universitas.class).list();
            return list;
        });

        return listUniv;
    }

}
